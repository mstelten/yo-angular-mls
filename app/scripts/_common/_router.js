photoApp.config(function ($routeProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	$routeProvider.when('/', {
		templateUrl: 'views/main.html',
		controller: 'MainCtrl'
	}).
//		when('/community', {
//			templateUrl: 'views/community.html',
//			controller: 'CommunityCtrl'
//		}).
		otherwise({redirectTo: '/'});
});