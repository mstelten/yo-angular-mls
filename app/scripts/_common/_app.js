'use strict';

var photoApp = angular.module('photoApp', [
  'ngAnimate',
  'ngRoute',
	'ngTouch'
]);
