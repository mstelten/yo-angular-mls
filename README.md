# **yo-angular-mls**

Customized AngularJS Yeoman Scaffolding

Based on official angularJS yeoman project

Adds some extra support into the mix:

-  LESS
-  Jade
-  Automatic file includes into index.html using 'sails-linker'
-  Add support for html5mode - Connect.options.middleware modrewrite allows for this & base url in header